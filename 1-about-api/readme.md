Aufgabe: The About API

1. Die Command-Zeile führt die API aus, im Browser wird der Playground von GraphQL angezeigt

2. Pros: Die GET Requests können vom Browser gecached werden. Für den Response werden die Daten dann direkt vom Cache wiedergegeben.

Cons: Je nach Browser und deren Version kann es zu Abweichungen kommen. Somit ist es nicht wirklich voraussehbar. GET Requests werden via URL ausgeführt was ein höheres Sicherheitsrisiko ist. Im Gegensatz dazu werden POST Requests nicht gecached.