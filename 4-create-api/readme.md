Aufgabe: Create API

1. Die beiden Methoden sind gleich. Die gemeinsamen Eigenschaften müssen zwischen "Issue" und "IssueInputs" wiederholt werden. Beim Hinzufügen von neuen Eigenschaften muss die Änderung an mehreren Stellen gemacht werden.

2. ast.kind wird zu Kind.INT. Die Console git somit einen String zurück.

3. Ein ungültiges Datum kann in einem gültigen String übergeben werden ohne dass ein Error auftaucht. Man kann dies erst nachvollziehen wenn ein Issue zurückgegeben wird. > Das Date-Object konnte nicht in einen String umgewandelt werden.

4. Ein Default-Value kann im Schema spezifiert werden. "type" = default-value 